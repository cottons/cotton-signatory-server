package cotton.signatory.services.servlets;

import javax.servlet.http.HttpServletRequest;

public class RequestInputMessage extends HttpInputMessage {

    public RequestInputMessage(HttpServletRequest request) {
        super(request);
    }

    @Override
    protected String getParameter(String name) {
        return request.getParameter(name);
    }

}
