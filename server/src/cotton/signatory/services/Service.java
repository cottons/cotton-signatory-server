package cotton.signatory.services;

import cotton.signatory.Configuration;
import cotton.signatory.DocumentManager;
import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;
import cotton.signatory.core.DefaultConfiguration;
import cotton.signatory.core.ErrorManager;
import cotton.signatory.core.ServerDocumentManager;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.logging.Logger;

public abstract class Service {
    private Configuration configuration;
    private DocumentManager documentManager;

    protected static final Logger logger = Logger.getLogger("cotton.signatory");  //$NON-NLS-1$

    protected static final String ConfigurationFile = "cotton-signatory-server.config"; //$NON-NLS-1$
    protected static class Operation {
        public static final String Store = "put";
        public static final String Retrieve = "get";

    }

    protected Configuration loadConfiguration(InputMessage input, OutputMessage output) throws IOException {

        if (configuration != null)
            return configuration;

        configuration = new DefaultConfiguration();
        return configuration;
    }

    protected String checkId(InputMessage input, OutputMessage output) throws IOException {
        String id = input.getId();

        if (id != null)
            return id;

        throwFailure(ErrorManager.ERROR_MISSING_DATA_ID, output);
        return null;
    }

    protected String checkOperation(InputMessage input, String expectedOperation, OutputMessage output) throws IOException {
        String operation = input.getOperation();

        if (operation != null && expectedOperation.equalsIgnoreCase(operation))
            return operation;

        throwFailure(operation == null ? ErrorManager.ERROR_MISSING_OPERATION_NAME : ErrorManager.ERROR_UNSUPPORTED_OPERATION_NAME, output);
        return null;
    }

    protected String checkSyntaxVersion(InputMessage input, OutputMessage output) throws IOException {
        String syntaxVersion = input.getSyntaxVersion();

        if (syntaxVersion != null)
            return syntaxVersion;

        throwFailure(ErrorManager.ERROR_MISSING_SYNTAX_VERSION, output);
        return null;
    }

    protected void throwFailure(String failureMessage, OutputMessage output) {
        String errorMessage = ErrorManager.genError(failureMessage, null);
        logger.severe(errorMessage); //$NON-NLS-1$
        output.write(errorMessage);
        throw new RuntimeException(errorMessage);
    }

    protected DocumentManager repository(Configuration configuration) {
        if (this.documentManager == null)
            this.documentManager = new ServerDocumentManager(configuration);
        return this.documentManager;
    }

}
