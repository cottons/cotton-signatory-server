package cotton.signatory.core;

import java.io.*;

public class StreamHelper {

	public static void close(InputStream stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (Exception e) {
			}
		}
	}

	public static void close(OutputStream stream) {
		if (stream != null) {
			try {
				stream.flush();
				stream.close();
			} catch (Exception e) {
			}
		}
	}

	public static void close(Reader reader) {
		if (reader != null) {
			try {
				reader.close();
			} catch (Exception e) {
			}
		}
	}

	public static void close(Writer writer) {
		if (writer != null) {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

	public static final int copyData(InputStream input, OutputStream output) throws IOException {
		int size = 0;
		int len;
		byte[] buff = new byte[16384];
		while ((len = input.read(buff)) > 0) {
			output.write(buff, 0, len);
			size += len;
		}
		return size;
	}

	public static final byte[] readBytes(InputStream input) throws IOException {
		if (input == null)
			return new byte[0];

		int nBytes;
		byte[] buffer = new byte[4096];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while ((nBytes = input.read(buffer)) != -1) {
			baos.write(buffer, 0, nBytes);
		}

		return baos.toByteArray();
	}

}
