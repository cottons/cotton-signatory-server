package cotton.signatory.services;

import com.google.gson.Gson;
import cotton.signatory.Configuration;
import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;
import cotton.signatory.core.ErrorManager;
import cotton.signatory.core.SignatureInfo;
import cotton.signatory.core.XadesSignatureHelper;
import cotton.signatory.services.servlets.BodyInputMessage;
import cotton.signatory.services.servlets.HttpOutputMessage;
import cotton.signatory.services.servlets.HttpService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SignatureService extends Service implements cotton.signatory.SignatureService {

    public void parseSignature(InputMessage input, OutputMessage output) {
        logger.info("Checking parameters"); //$NON-NLS-1$

        checkSignature(input, output);

        SignatureInfo info = new XadesSignatureHelper().getInfo(input.getSignature());
        output.write(new Gson().toJson(info));
    }

    private void checkSignature(InputMessage input, OutputMessage output) {
        if (input.getSignature() != null)
            return;

        logger.warning(String.format("Signature not found", input.getId()));
        throwFailure(ErrorManager.genError(ErrorManager.SIANI_ERROR_MISSING_SIGNATURE, null), output);
    }

}
