package cotton.signatory.services.servlets;

import cotton.signatory.Configuration;
import cotton.signatory.DocumentManager;
import cotton.signatory.core.DefaultConfiguration;
import cotton.signatory.core.ErrorManager;
import cotton.signatory.core.ServerDocumentManager;
import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.logging.Logger;

public abstract class HttpService extends HttpServlet {

    protected static final Logger logger = Logger.getLogger("cotton.signatory");  //$NON-NLS-1$

}
