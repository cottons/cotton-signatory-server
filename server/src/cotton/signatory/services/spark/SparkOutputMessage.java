package cotton.signatory.services.spark;

import cotton.signatory.OutputMessage;
import cotton.signatory.core.MimeTypes;
import spark.Response;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

public class SparkOutputMessage implements OutputMessage {
    protected final Response response;

    public SparkOutputMessage(Response response) {
        this.response = response;
    }

    public void write(String content) {
        try {
            HttpServletResponse raw = response.raw();
            final PrintWriter out = raw.getWriter();

            raw.setHeader("Access-Control-Allow-Origin", "*"); //$NON-NLS-1$ //$NON-NLS-2$
            raw.setContentType("text/plain"); //$NON-NLS-1$
            raw.setCharacterEncoding("utf-8"); //$NON-NLS-1$

            out.println(content);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(File file) {
        byte[] content;

        try {
            content = readFile(file);
            HttpServletResponse raw = response.raw();
            raw.setContentType(MimeTypes.getInstance().getFromFile(file));
            raw.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(byte[] content) {
        try {
            HttpServletResponse raw = response.raw();
            raw.setContentLength(content.length);
            raw.getOutputStream().write(content);
            raw.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String filename, byte[] content) {
        HttpServletResponse raw = response.raw();
        raw.setContentType(MimeTypes.getInstance().getFromFilename(filename));
        raw.setHeader("Content-Disposition", "attachment; filename=" + filename);
        this.write(content);
    }

    private byte[] readFile(File file) throws IOException {
        return Files.readAllBytes(file.toPath());
    }

}
