package cotton.signatory.services.spark;

import cotton.signatory.services.servlets.HttpInputMessage;
import spark.Request;

import javax.servlet.http.HttpServletRequest;

public class RequestInputMessage extends SparkInputMessage {

    public RequestInputMessage(Request request) {
        super(request);
    }

    @Override
    protected String getParameter(String name) {
        return request.raw().getParameter(name);
    }

}
