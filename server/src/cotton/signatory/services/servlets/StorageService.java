package cotton.signatory.services.servlets;

import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StorageService extends HttpService {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("New storage request");

        InputMessage input = new BodyInputMessage(request);
        OutputMessage output = new HttpOutputMessage(response);

        new cotton.signatory.services.StorageService().store(input, output);
    }

}
