package cotton.signatory.services.servlets;

import cotton.signatory.OutputMessage;
import cotton.signatory.core.MimeTypes;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

public class HttpOutputMessage implements OutputMessage {
    protected final HttpServletResponse response;

    public HttpOutputMessage(HttpServletResponse response) {
        this.response = response;
    }

    public void write(String content) {
        try {
            final PrintWriter out = response.getWriter();

            response.setHeader("Access-Control-Allow-Origin", "*"); //$NON-NLS-1$ //$NON-NLS-2$
            response.setContentType("text/plain"); //$NON-NLS-1$
            response.setCharacterEncoding("utf-8"); //$NON-NLS-1$

            out.println(content);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(File file) {
        byte[] content;

        try {
            content = readFile(file);
            this.response.setContentType(MimeTypes.getInstance().getFromFile(file));
            this.response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(byte[] content) {
        try {
            this.response.setContentLength(content.length);
            this.response.getOutputStream().write(content);
            this.response.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String filename, byte[] content) {
        this.response.setContentType(MimeTypes.getInstance().getFromFilename(filename));
        this.response.setHeader("Content-Disposition", "attachment; filename=" + filename);
        this.write(content);
    }

    private byte[] readFile(File file) throws IOException {
        return Files.readAllBytes(file.toPath());
    }

}
