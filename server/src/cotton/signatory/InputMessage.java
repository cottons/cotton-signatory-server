package cotton.signatory;

import java.io.File;

public interface InputMessage {
    String getId();
    String getOperation();
    String getSyntaxVersion();
    String getData();
    String getSignature();
    String getAppName();

    boolean downloadApp();
    String getOperatingSystem();
    String getRetrieveUrl();

    String getDownloadFileUrl(File file);
}
