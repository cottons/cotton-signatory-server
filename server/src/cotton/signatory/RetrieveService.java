package cotton.signatory;

public interface RetrieveService {
    void retrieve(InputMessage input, OutputMessage output);
}
