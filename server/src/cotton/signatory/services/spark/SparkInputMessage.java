package cotton.signatory.services.spark;

import cotton.signatory.InputMessage;
import spark.Request;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

public abstract class SparkInputMessage implements InputMessage {
    protected final Request request;

    public SparkInputMessage(Request request) {
        this.request = request;
    }

    public String getId() {
        return getParameter(Parameter.Id);
    }

    public String getOperation() {
        return getParameter(Parameter.Operation);
    }

    public String getSyntaxVersion() {
        return getParameter(Parameter.SyntaxVersion);
    }

    public String getData() {
        return getParameter(Parameter.Data);
    }

    public String getSignature() {
        return getParameter(Parameter.Signature);
    }

    public String getAppName() {
        return request.raw().getSession().getServletContext().getServletContextName();
    }

    public String getCertificate() {
        return getParameter(Parameter.Certificate);
    }

    public String getAlgorithm() {
        return getParameter(Parameter.Algorithm);
    }

    public String getFormat() {
        return getParameter(Parameter.Format);
    }

    public String getDesKey() {
        return "12345678";
    }

    public String getDocumentId() {
        return getParameter(Parameter.Document);
    }

    public String getSessionData() {
        return getParameter(Parameter.SessionData);
    }

    public String getExtraParams() {
        return getParameter(Parameter.ExtraParams);
    }

    public boolean downloadApp() {
        return request.raw().getRequestURL().toString().contains("/app");
    }

    public String getOperatingSystem() {
        return getParameter(Parameter.OperatingSystem);
    }

    public boolean storeSignedPdf() {
        return request.raw().getRequestURL().toString().contains("/store-signed-pdf");
    }

    public String getRetrieveUrl() {
        return getBaseUrl() + "/retrieve";
    }

    public String getSignedPdfStorageUrl() {
        return getBaseUrl() + "/store-signed-pdf";
    }

    public String getDownloadFileUrl(File file) {
        return getBaseUrl() + "/file?id=" + file.toPath().getFileName();
    }

    private String getBaseUrl() {
        String url = request.raw().getRequestURL().toString();
        return url.substring(0, url.lastIndexOf("/"));
    }

    protected static class Parameter {
        protected static final String Operation = "op";
        protected static final String Id = "id";
        protected static final String SyntaxVersion = "v";
        protected static final String Data = "dat";
        protected static final String Signature = "sig";
        protected static final String Certificate = "cert";
        protected static final String Algorithm = "algo";
        protected static final String Format = "format";
        protected static final String Document = "doc";
        protected static final String SessionData = "session";
        protected static final String ExtraParams = "params";
        protected static final String OperatingSystem = "os";
    }

    protected abstract String getParameter(String name);

}
