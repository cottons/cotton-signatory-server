package cotton.signatory.core;

import es.mityc.firmaJava.libreria.xades.ResultadoValidacion;
import es.mityc.firmaJava.libreria.xades.ValidarFirmaXML;
import es.mityc.firmaJava.libreria.xades.errores.FirmaXMLError;
import es.mityc.javasign.ts.TimeStampValidator;
import org.apache.commons.codec.binary.Base64;
import org.monet.encrypt.extractor.CertificateExtractor;
import org.monet.encrypt.extractor.ExtractorUser;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.security.cert.X509Certificate;
import java.util.List;

public class XadesSignatureHelper implements SignatureHelper {

    public SignatureInfo getInfo(String signature) {
        final ExtractorUser user;
        try {
            user = extractInfoFrom(certificateOf(signature));
            return infoOf(user);
        } catch (FirmaXMLError firmaXMLError) {
            return null;
        }
    }

    public X509Certificate getCertificate(String signature) {
        try {
            return certificateOf(signature);
        } catch (FirmaXMLError firmaXMLError) {
            return null;
        }
    }

    private X509Certificate certificateOf(String signature) throws FirmaXMLError {
        Document documentSignature = signatureDocumentOf(signature);
        ResultadoValidacion result = loadSignature(documentSignature);

        if (!result.isValidate())
            return null;

        return (X509Certificate) result.getDatosFirma().getCadenaFirma().getCertificates().get(0);
    }

    private Document signatureDocumentOf(String signature) {
        try {
            String xmlSignature = new String(Base64.decodeBase64(signature), "UTF-8");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);

            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            return documentBuilder.parse(new ByteArrayInputStream(xmlSignature.getBytes()));
        } catch (Exception e) {
            return null;
        }
    }

    private ResultadoValidacion loadSignature(Document documentSignature) throws FirmaXMLError {
        ValidarFirmaXML vXml = new ValidarFirmaXML();
        List<ResultadoValidacion> results = vXml.validar(documentSignature, "./", null, new TimeStampValidator());
        return results.get(0);
    }

    private ExtractorUser extractInfoFrom(X509Certificate certificate) {
        CertificateExtractor extractor = new CertificateExtractor();
        return extractor.extractUser(certificate);
    }

    private SignatureInfo infoOf(final ExtractorUser user) {
        return new SignatureInfo(user.getUsername(), user.getFullname(), user.getEmail());
    }
}
