<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getRequestURL().toString();
    baseUrl = baseUrl.substring(0, baseUrl.lastIndexOf("/"));
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>cotton-signatory digital signatures sample</title>
    <script src="webcomponents/webcomponentsjs/webcomponents-lite.js"></script>
    <link rel="import" href="webcomponents/paper-input/paper-input.html">
    <link rel="import" href="webcomponents/paper-button/paper-button.html">
    <link rel="import" href="webcomponents/cotton-signatory/cotton-signatory-signer.html">
</head>
<body>
    <style is="custom-style">
        body {
            @apply(--layout-vertical);
            font-family: RobotoDraft, 'Helvetica Neue', Helvetica, Arial;
            background: #efefef;
            margin: 0 20px 10px;
        }

        .dialog {
            align-items:center;
            -webkit-align-items: center;
            justify-content:center;
            -webkit-justify-content:center;
            width: 600px;
            min-width: 300px;
            padding: 20px 10px;
            margin-top: 10px;
            background: white;
            border: 1px solid #efefef;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            box-shadow: 2px 2px 5px #808080;
            -webkit-box-shadow: 2px 2px 5px #808080;
            -moz-box-shadow: 2px 2px 5px #808080;
        }

        .simple {
            background: none;
            border: 0;
            box-shadow: none;
            padding: 0;
            margin: 0;
        }

        h1, h2, .dialog {
            margin-left: 10px;
            margin-bottom: 2px;
        }

        p {
            margin: 15px 10px;
        }

        a {
            text-decoration: none;
            font-size: 20px;
        }

        paper-button {
            margin: 10px 0;
            float: right;
            background: #4285f4;
            color: #fff;
        }

        .simple paper-button {
            float: none;
        }

        .home {
            right: 0;
            text-align: right;
            margin-top: 10px;
            position: absolute;
            margin-right: 20px;
        }

        .result {
            width: 630px;
            word-wrap: break-word;
            height: 200px;
            overflow: auto;
            margin-top: 20px;
            margin-left: 10px;
            display: none;
        }

    </style>

    <a href="index.html" class="home">home</a>

    <cotton-signatory-signer language="en" download-url="<%=baseUrl%>/cotton-signatory/app"
                             storage-url="<%=baseUrl%>/cotton-signatory/store"
                             retrieve-url="<%=baseUrl%>/cotton-signatory/retrieve"></cotton-signatory-signer>

    <h1>Sign text sample</h1>
    <div class="dialog">
        <paper-input id="signText" label="text to sign" value="lorem ipsum dolor sit amet"></paper-input>
        <paper-button id="signTextButton">sign text</paper-button>
    </div>
    <div id="signTextResult" class="result"></div>

    <h1>Sign document sample</h1>
    <h2>from local (it must be a pdf file)</h2>
    <div class="dialog simple"><paper-button id="signDocumentFromLocalButton">sign document</paper-button></div>
    <div id="signDocumentFromLocalResult" class="result"></div>

    <!--
    <h2>from url (it must be located at intermediate server)</h2>
    <div class="dialog">
        <paper-input id="signDocumentFromUrl" label="identifier of file to sign (alphanumeric identifier)" value="filetosign"></paper-input>
        <paper-button id="signDocumentFromUrlButton">sign document</paper-button>
    </div>
    <div id="signDocumentFromUrlResult" class="result"></div>
    -->

    <script>
        var signer = document.querySelector("cotton-signatory-signer");

        document.querySelector("#signTextButton").addEventListener("click", function(event) {
            hideResult("signTextResult");

            var text = document.querySelector("#signText").value;
            if (text === "") return;

            signer.init();
            signer.signText(text, function(signature, certificate) {
                showResult("signTextResult", ["Signature: " + signature, "Certificate: " + certificate]);
            }, function(errorCode, errorMessage) {
                showResult("signTextResult", [errorMessage]);
            });
        });

        document.querySelector("#signDocumentFromLocalButton").addEventListener("click", function(event) {
            hideResult("signDocumentFromLocalResult");

            signer.init();
            signer.signLocalDocument(function(signature, certificate) {
                showResult("signDocumentFromLocalResult", ["Signature: " + signature, "Certificate: " + certificate]);
            }, function(errorCode, errorMessage) {
                showResult("signDocumentFromLocalResult", [errorMessage]);
            });
        });

        /*document.querySelector("#signDocumentFromUrlButton").addEventListener("click", function(event) {
            hideResult("signDocumentFromUrlResult");

            var documentUrl = document.querySelector("#signDocumentFromUrl").value;

            signer.signDocument(documentUrl, function(signature, certificate) {
                showResult("signDocumentFromUrlResult", ["Signature: " + signature, "Certificate: " + certificate]);
            }, function(errorCode, errorMessage) {
                showResult("signDocumentFromUrlResult", [errorMessage]);
            });
        });*/

        function showResult(containerId, content) {
            var container = document.querySelector("#" + containerId);
            container.style.display = "block";
            container.innerHTML = "";
            for (var i=0; i<content.length; i++) {
                if (i != 0) container.innerHTML += "<br/><br/>";
                container.innerHTML += "<div>" + content[i] + "</div>";
            }
        };

        function hideResult(containerId) {
            document.querySelector("#" + containerId).style.display = "none";
        }

    </script>

</body>
</html>