package cotton.signatory;

public interface Configuration {
	String getSignaturesDirectory();
	long getSignaturesExpirationTime();
	String getRepositoryDirectory();
	String getClientApplicationFilename(String operatingSystem);
}
