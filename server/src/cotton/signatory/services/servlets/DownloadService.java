package cotton.signatory.services.servlets;

import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadService extends HttpService {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Download client application request");

        InputMessage input = new RequestInputMessage(request);
        OutputMessage output = new HttpOutputMessage(response);

        new cotton.signatory.services.DownloadService().download(input, output);
    }

}
