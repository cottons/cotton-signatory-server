package cotton.signatory.services.servlets;

import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignatureService extends HttpService {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) {
        logger.info("New retrieve signature request");

        InputMessage input = new BodyInputMessage(request);
        OutputMessage output = new HttpOutputMessage(response);

        new cotton.signatory.services.SignatureService().parseSignature(input, output);

        logger.info("Retrieve signature info finished");
    }

}
