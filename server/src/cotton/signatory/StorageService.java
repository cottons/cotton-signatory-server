package cotton.signatory;

public interface StorageService {
    boolean store(InputMessage input, OutputMessage output);
}
