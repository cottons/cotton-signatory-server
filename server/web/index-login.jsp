<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getRequestURL().toString();
    baseUrl = baseUrl.substring(0, baseUrl.lastIndexOf("/"));
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>cotton-signatory login sample</title>
    <script src="webcomponents/webcomponentsjs/webcomponents-lite.js"></script>
    <link rel="import" href="webcomponents/paper-input/paper-input.html">
    <link rel="import" href="webcomponents/paper-button/paper-button.html">
    <link rel="import" href="webcomponents/cotton-signatory/cotton-signatory-login.html">
    <link rel="import" href="webcomponents/iron-ajax/iron-ajax.html">
</head>
<body>
    <style is="custom-style">
        body {
            @apply(--layout-vertical);
            font-family: RobotoDraft, 'Helvetica Neue', Helvetica, Arial;
            background: #efefef;
            margin: 0 20px 10px;
        }

        .dialog {
            align-items:center;
            -webkit-align-items: center;
            justify-content:center;
            -webkit-justify-content:center;
            width: 500px;
            min-width: 300px;
            padding: 20px 10px;
            background: white;
            border: 1px solid #efefef;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            box-shadow: 2px 2px 5px #808080;
            -webkit-box-shadow: 2px 2px 5px #808080;
            -moz-box-shadow: 2px 2px 5px #808080;
            margin: 100px auto;
        }

        h1 {
            margin-left: 10px;
            margin-bottom: 0;
        }

        p {
            margin: 15px 10px;
        }

        a {
            text-decoration: none;
            font-size: 20px;
        }

        #response, #responseFailure {
            color: blue;
            font-size: 18px;
            margin: 70px 0 0;
            display: none;
            text-align: center;
        }

        #responseFailure {
            color: red;
        }

        paper-button {
            margin: 10px 0;
            float: right;
            background: #4285f4;
            color: #fff;
        }

        .home {
            right: 0;
            text-align: right;
            margin-top: 10px;
            position: absolute;
            margin-right: 20px;
        }

    </style>

    <a href="index.html" class="home">home</a>

    <h1>Example application using @firma for log-in users with certificate</h1>
    <div class="dialog centered">
        <div>
            <iron-ajax method="post" url="<%=baseUrl%>/cotton-signatory/signature" content-type="application/text"></iron-ajax>
            <cotton-signatory-login language="en" download-url="<%=baseUrl%>/cotton-signatory/app"
                                    storage-url="<%=baseUrl%>/cotton-signatory/store"
                                    retrieve-url="<%=baseUrl%>/cotton-signatory/retrieve"></cotton-signatory-login>
        </div>
        <div id="response" class="flex"></div>
        <div id="responseFailure" class="flex">login failure!!</div>
    </div>

    <script>
        document.querySelector("cotton-signatory-login").addEventListener("login", function(event) {
            var ajax = document.querySelector("iron-ajax");
            ajax.body = "sig=" + event.detail.signature;
            ajax.generateRequest();
        });

        document.querySelector("cotton-signatory-login").addEventListener("loginFailure", function(event) {
            var responseFailure = document.querySelector("#responseFailure");
            responseFailure.style.display = "block";
            if (event.detail.message !== "")
                responseFailure.innerHTML = event.detail.message;
        });

        document.querySelector("iron-ajax").addEventListener("response", function(event) {
            var response = document.querySelector("#response");
            response.style.display = "block";
            response.innerHTML = event.detail.response.fullName + " tries to login";
        });
    </script>

</body>
</html>