package cotton.signatory.services;

import cotton.signatory.Configuration;
import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;
import cotton.signatory.core.StreamHelper;

import java.io.IOException;
import java.io.InputStream;

public class DownloadService extends Service implements cotton.signatory.DownloadService {

    public void download(InputMessage input, OutputMessage output) {

        try {
            Configuration configuration = loadConfiguration(input, output);

            if (input.downloadApp()) {
                String operatingSystem = input.getOperatingSystem();
                String filename = configuration.getClientApplicationFilename(operatingSystem);
                InputStream clientApplicationResource = repository(configuration).getClientApplicationResource(operatingSystem);
                output.write(filename, StreamHelper.readBytes(clientApplicationResource));
                return;
            }

            output.write(repository(configuration).getRepositoryDocument(input.getId()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
