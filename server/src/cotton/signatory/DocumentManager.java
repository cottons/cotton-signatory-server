package cotton.signatory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface DocumentManager {
    InputStream getClientApplicationResource(String operatingSystem);
    File getRepositoryDocument(String key);

    File getSignature(String key);
    void putSignature(String key, byte[] signature) throws IOException;
    boolean isSignatureExpired(String key);
    void cleanExpiredSignatures();
}
