package cotton.signatory.core;

import java.security.cert.X509Certificate;

public interface SignatureHelper {
    SignatureInfo getInfo(String signature);
    X509Certificate getCertificate(String signature);
}
