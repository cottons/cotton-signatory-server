package cotton.signatory;

public interface SignatureService {
    void parseSignature(InputMessage input, OutputMessage output);
}
