package cotton.signatory.services;

import cotton.signatory.Configuration;
import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;
import cotton.signatory.core.ErrorManager;
import cotton.signatory.core.StreamHelper;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class StorageService extends Service implements cotton.signatory.StorageService {

    public boolean store(InputMessage input, OutputMessage output) {
        OutputStream fos = null;
        String id = null;
        boolean stored = true;

        try {
            logger.info("Checking parameters"); //$NON-NLS-1$
            checkOperation(input, Operation.Store, output);
            checkSyntaxVersion(input, output);
            checkId(input, output);
            checkData(input, output);

            id = input.getId();
            logger.info("Request for saving file with id: " + id); //$NON-NLS-1$

            byte[] signature = parseData(input);
            if (signature != null) {
                Configuration configuration = loadConfiguration(input, output);
                repository(configuration).putSignature(input.getId(), signature);
            }
            else
                stored = false;

        } catch (final IOException e) {
            stored = false;
        } finally {
            StreamHelper.close(fos);
        }

        if (!stored)
            logger.severe("Request for saving file " + id + " FAILURE"); //$NON-NLS-1$
        else
            logger.info("Request for saving file " + id + " FINISHED"); //$NON-NLS-1$

        output.write(stored ? "OK" : ErrorManager.genError(ErrorManager.ERROR_COMMUNICATING_WITH_WEB, null));

        return true;
    }

    private byte[] parseData(InputMessage input) {
        return input.getData().getBytes();
    }

    protected String checkData(final InputMessage input, OutputMessage output) throws IOException {
        String data = input.getData();

        if (data != null)
            return data;

        InputMessage inputWhenError = new InputMessage() {
            public String getId() {
                return input.getId();
            }

            public String getOperation() {
                return input.getOperation();
            }

            public String getSyntaxVersion() {
                return input.getSyntaxVersion();
            }

            public String getData() {
                return ErrorManager.genError(ErrorManager.ERROR_MISSING_DATA, null);
            }

            public String getSignature() {
                return input.getSignature();
            }

            public String getAppName() {
                return input.getAppName();
            }

            public String getRetrieveUrl() {
                return input.getRetrieveUrl();
            }

            public boolean downloadApp() {
                return input.downloadApp();
            }

            public String getOperatingSystem() {
                return input.getOperatingSystem();
            }

            public String getDownloadFileUrl(File file) {
                return input.getDownloadFileUrl(file);
            }
        };
        store(inputWhenError, output);
        throwFailure(ErrorManager.ERROR_MISSING_DATA, output);
        return null;
    }

}
