package cotton.signatory;

import java.io.File;

public interface OutputMessage {
    void write(String content);
    void write(File file);
    void write(byte[] content);
    void write(String name, byte[] content);
}
