package cotton.signatory.services;

import cotton.signatory.Configuration;
import cotton.signatory.InputMessage;
import cotton.signatory.OutputMessage;
import cotton.signatory.core.ErrorManager;
import cotton.signatory.core.StreamHelper;
import cotton.signatory.services.servlets.HttpOutputMessage;
import cotton.signatory.services.servlets.HttpService;
import cotton.signatory.services.servlets.RequestInputMessage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

public class RetrieveService extends Service implements cotton.signatory.RetrieveService {

    public void retrieve(InputMessage input, OutputMessage output) {

        try {
            Configuration configuration = loadConfiguration(input, output);

            logger.info("Checking parameters"); //$NON-NLS-1$

            checkOperation(input, Operation.Retrieve, output);
            checkSyntaxVersion(input, output);
            checkId(input, output);

            logger.info("Request for loading file with id: " + input.getId()); //$NON-NLS-1$

            File file = repository(configuration).getSignature(input.getId());

            if (!file.exists()) {
                output.write(ErrorManager.genError(ErrorManager.ERROR_INVALID_DATA_ID, null));
                return;
            }

            checkIsFile(file, input, output);
            checkCanReadFile(file, input, output);
            checkIsExpired(file, input, output, configuration);

            writeOutput(file, input, output, configuration);
            removeFile(file, input, configuration);

            repository(configuration).cleanExpiredSignatures();

            logger.info(String.format("File %s recovered", configuration.getSignaturesDirectory() + "/" + input.getId()));
        }
        catch (IOException e) {
            throwFailure(ErrorManager.ERROR_INVALID_DATA, output);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    private void writeOutput(File file, InputMessage input, OutputMessage output, Configuration configuration) throws IOException, GeneralSecurityException {
        FileInputStream stream = null;

        try {
            stream = new FileInputStream(file);
            output.write(new String(StreamHelper.readBytes(stream), "UTF-8"));
        } finally {
            StreamHelper.close(stream);
        }
    }

    private void checkIsFile(File file, InputMessage input, OutputMessage output) throws IOException {
        if (file.isFile())
            return;

        removeFile(file, input, loadConfiguration(input, output));
        logger.warning(String.format("File with identifier %s is not a valid file", input.getId()));
        throwFailure(ErrorManager.genError(ErrorManager.ERROR_INVALID_DATA_ID, null), output);
    }

    private void checkCanReadFile(File file, InputMessage input, OutputMessage output) throws IOException {
        if (file.canRead())
            return;

        removeFile(file, input, loadConfiguration(input, output));
        logger.warning(String.format("Cant read file with identifier %s", input.getId()));
        throwFailure(ErrorManager.genError(ErrorManager.ERROR_INVALID_DATA_ID, null), output);
    }

    private void checkIsExpired(File file, InputMessage input, OutputMessage output, Configuration configuration) throws IOException {
        if (!repository(configuration).isSignatureExpired(input.getId()))
            return;

        removeFile(file, input, loadConfiguration(input, output));
        logger.warning(String.format("File with identifier %s is expired", input.getId()));
        throwFailure(ErrorManager.genError(ErrorManager.ERROR_INVALID_DATA_ID, null), output);
    }

    private void removeFile(File file, InputMessage input, Configuration configuration) {

        if (!file.exists())
            return;

        if (!file.isFile())
            return;

        file.delete();
    }

}
