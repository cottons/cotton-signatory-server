package cotton.signatory;

public interface DownloadService {
    void download(InputMessage input, OutputMessage output);
}
