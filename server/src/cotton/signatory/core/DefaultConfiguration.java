package cotton.signatory.core;

import cotton.signatory.Configuration;

import java.io.*;
import java.util.logging.Logger;

public class DefaultConfiguration implements Configuration {
	private static String SIGNATURES_DIR;
	private static String REPOSITORY_DIR;
	private static final String CLIENT_APPLICATION_FILENAME = "AutoFirma1.4.1";
	private static final long DEFAULT_EXPIRATION_TIME = 5000; // 5 seconds

	static {
		try {
			SIGNATURES_DIR = System.getProperty("java.io.tmpdir") + "/cotton-signatory-signatures"; //$NON-NLS-1$
			REPOSITORY_DIR = System.getProperty("java.io.tmpdir") + "/cotton-signatory-repositories"; //$NON-NLS-1$
		} catch (Exception e) {
			try {
				SIGNATURES_DIR = File.createTempFile("tmp" + "/cotton-signatory-signatures", null).getParentFile().getAbsolutePath(); //$NON-NLS-1$
				REPOSITORY_DIR = File.createTempFile("tmp" + "/cotton-signatory-repository", null).getParentFile().getAbsolutePath(); //$NON-NLS-1$
			} catch (IOException e1) {
				e1.printStackTrace();
				SIGNATURES_DIR = null;
				REPOSITORY_DIR = null;
				Logger.getLogger("cotton.signatory").warning("No se ha podido cargar un directorio temporal por defecto, se debera configurar expresamente en el fichero de propiedades"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}
	
	public String getSignaturesDirectory() {
		return SIGNATURES_DIR;
	}

	public long getSignaturesExpirationTime() {
		return DEFAULT_EXPIRATION_TIME;
	}

	public String getClientApplicationFilename(String operatingSystem) {
		return CLIENT_APPLICATION_FILENAME + "." + extensionOf(operatingSystem);
	}

	private String extensionOf(String os) {
		if (os.contains("macos")) return "app";
		if (os.contains("linux_rpm")) return "rpm";
		if (os.contains("linux_deb")) return "deb";
		if (os.contains("win")) return "exe";
		return null;
	}

	public String getRepositoryDirectory() {
		return REPOSITORY_DIR;
	}

}
