package cotton.signatory.core;

import cotton.signatory.Configuration;
import cotton.signatory.DocumentManager;

import java.io.*;

public class ServerDocumentManager implements DocumentManager {
    private final Configuration configuration;

    public ServerDocumentManager(Configuration configuration) {
        this.configuration = configuration;
        checkSignaturesDirectory();
    }

    private void checkSignaturesDirectory() {
        File signaturesDirectory = new File(configuration.getSignaturesDirectory());
        if (!signaturesDirectory.exists())
            signaturesDirectory.mkdirs();
    }

    public InputStream getClientApplicationResource(String operatingSystem) {
        String clientApplicationFilename = configuration.getClientApplicationFilename(operatingSystem);
        return ServerDocumentManager.class.getResourceAsStream("/" + clientApplicationFilename);
    }

    public File getRepositoryDocument(String key) {
        return new File(configuration.getRepositoryDirectory() + "/" + key);
    }

    public boolean existsRepositoryDocument(String key) {
        File repositoryDirectory = new File(configuration.getRepositoryDirectory());
        return getRepositoryDocument(key).getParent().equals(repositoryDirectory.getAbsolutePath());
    }

    public File getSignature(String key) {
        return new File(configuration.getSignaturesDirectory(), key);
    }

    public void putSignature(String key, byte[] signature) throws IOException {
        saveFile(new File(configuration.getSignaturesDirectory(), key), signature);
    }

    public boolean isSignatureExpired(String key) {
        if (existsRepositoryDocument(key)) return false;
        File file = getSignature(key);
        return (System.currentTimeMillis() - file.lastModified()) > configuration.getSignaturesExpirationTime();
    }

    public void cleanExpiredSignatures() {
        File signaturesDirectory = new File(configuration.getSignaturesDirectory());

        if (!signaturesDirectory.exists())
            return;

        for (File file : signaturesDirectory.listFiles())
            try {
                if ((file.exists()) && (file.isFile()) && (isSignatureExpired(file.toPath().getFileName().toString())))
                    file.delete();
            }
            catch (Exception exception) {
            }
    }

    private void saveFile(File file, byte[] content) throws IOException {
        ByteArrayInputStream signatureStream = new ByteArrayInputStream(content);

        try {
            if (!file.exists())
                file.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(file);
            StreamHelper.copyData(signatureStream, fileOutputStream);
        }
        finally {
            StreamHelper.close(signatureStream);
        }
    }

}
